/*
 *
 * University of Luxembourg
 * Laboratory of Algorithmics, Cryptology and Security (LACS)
 *
 * FELICS - Fair Evaluation of Lightweight Cryptographic Systems
 *
 * Copyright (C) 2015 University of Luxembourg
 *
 * Written in 2015 by Daniel Dinu <dumitru-daniel.dinu@uni.lu> and 
 * Yann Le Corre <yann.lecorre@uni.lu>
 *
 * This file is part of FELICS.
 *
 * FELICS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FELICS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stddef.h>
#include <stdint.h>

#include "felics/cipher.h"
#include "felics/common.h"
#include "api.h"
#include "crypto_aead.h"

#if defined(NRF52840)
#include "app_uart.h"
#include "app_error.h"
#include "nrf.h"
#include "bsp.h"
#include "nrf_uart.h"
#endif /* NRF52840 */

#if defined(STM32L053) && defined(MEASURE_CYCLE_COUNT) && \
	(MEASURE_CYCLE_COUNT_ENABLED == MEASURE_CYCLE_COUNT)
#include <stdio.h>
#include <stdint.h>
#include "usart.h"
#include "gpio.h"
#include "error_handler.h"
#include "system_clock.h"
#include "cycleCount.h"
#endif /* STM32L053 & MEASURE_CYCLE_COUNT */

#if defined(STM32L053) && defined(DEBUG) && (DEBUG_LOW == (DEBUG_LOW & DEBUG))
#include <stdio.h>
#include <stdint.h>
#include "usart.h"
#include "gpio.h"
#include "error_handler.h"
#include "system_clock.h"
#endif /* STM32L053 & DEBUG */


/* Implementation-checking program. */
int main()
{
        RAM_DATA_BYTE state[MAXTEST_BYTES_M];
        size_t mlen;

        RAM_DATA_BYTE key[CRYPTO_KEYBYTES];

        /* Contains the ciphertext, followed by the tag. */
        RAM_DATA_BYTE c[MAXTEST_BYTES_M+CRYPTO_ABYTES];
        size_t clen;

        RAM_DATA_BYTE ad[MAXTEST_BYTES_AD];

        RAM_DATA_BYTE npub[CRYPTO_NPUBBYTES];

        InitializeDevice();

        InitializeState(state);
        DisplayVerifyData(state, MAXTEST_BYTES_M, PLAINTEXT_NAME);

        InitializeKey(key);
        DisplayVerifyData(key, CRYPTO_KEYBYTES, KEY_NAME);

        InitializeAd(ad, MAXTEST_BYTES_AD);
        DisplayVerifyData(ad, MAXTEST_BYTES_AD, ASSOCIATED_NAME);

        InitializeNpub(npub);

        BEGIN_ENCRYPTION();
        crypto_aead_encrypt(c, &clen, state, sizeof(state), ad, sizeof(ad), npub, key);
        END_ENCRYPTION();

        DisplayVerifyData(c, MAXTEST_BYTES_M + CRYPTO_ABYTES, CIPHERTEXT_NAME);

        BEGIN_DECRYPTION();
        int valid = crypto_aead_decrypt(state, &mlen, c, clen, ad, sizeof(ad), npub, key);
        END_DECRYPTION();

        DisplayVerifyData(state, MAXTEST_BYTES_M, PLAINTEXT_NAME);

        DONE();
        StopDevice();

        return valid;
}
