#ifndef PARAMETERS_H
#define PARAMETERS_H

#define KEY_LENGTH_BITS 256
#define ROUNDS          42

#define TWEAK_LENGTH_BITS 192

#endif /* PARAMETERS_H */
