#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

void permutation(uint8_t* S, int start, int rounds);

#endif /* UTILS_H */
