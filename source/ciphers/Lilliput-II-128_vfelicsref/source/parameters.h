#ifndef PARAMETERS_H
#define PARAMETERS_H

#define KEY_LENGTH_BITS 128
#define ROUNDS          32

#define TWEAK_LENGTH_BITS 128

#endif /* PARAMETERS_H */
