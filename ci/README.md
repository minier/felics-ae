This folder contains scripts run by our Continuous Integration system
to produce artifacts used in other projects, such as LaTeX tables for
the NIST submission specification of Lilliput-AE, or HTML tables for
paclido.fr.
