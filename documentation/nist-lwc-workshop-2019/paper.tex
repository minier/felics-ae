\documentclass{article}

\usepackage{authblk}
\usepackage{fancyvrb}
\usepackage{float}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}     % Needed before LaTeX 2018.
\usepackage{lmodern}            % To get bold-teletype.
\usepackage{multirow}
\usepackage[binary-units]{siunitx}
\usepackage{xcolor}


\title{
  FELICS-AE: a framework to benchmark lightweight authenticated block ciphers
}

\author[*]{Kévin Le Gouguec}

\affil[*]{
  Airbus CyberSecurity -
  ZA Clef Saint-Pierre,
  1 Bd Jean Moulin,
  CS 40001,
  MetaPole,
  78996 ÉLANCOURT Cedex -
  France -
  \href{mailto:kevin.legouguec@airbus.com}{kevin.legouguec@airbus.com}
}


\begin{document}

\maketitle

\section{Introduction}
\label{sec:intro}

The CAESAR competition~\cite{CAESAR:submissions} and the NIST
Lightweight Cryptography Standardization Process~\cite{NIST:LWC} have
brought to light several new Authenticated Encryption with Associated
Data (AEAD) algorithms dedicated to ``lightweight'' use-cases.  In
these use-cases, target devices are strongly constrained in terms of
computing resources: they have limited volatile memory (RAM) and
non-volatile memory (ROM), their processors operate at low frequencies
and feature few registers, they may only be able to draw power from a
battery that can neither be recharged nor replaced, etc.

These devices thus have very few resources to spare on security.  This
implies that the aforementioned algorithms must be selected not only
for their robustness, but also according to their efficiency.  Given
two encryption schemes with equivalent security, the scheme which
leaves the target device more resources to perform its designed
function will be preferred.

Thus measuring the performance of these algorithms is an integral part
of the selection process carried out in~\cite{NIST:LWC}.  In this
paper, we present FELICS-AE, an adaptation of the FELICS
framework~\cite{FELICS:paper} dedicated to AEAD schemes, which we use
to assess the performance of our candidate
\textsc{Lilliput-AE}~\cite{NIST:Lilliput-AE}.  We have released this
framework on a public Git repository\cite{INRIA:FELICS-AE}.

First, in section~\ref{sec:felics}, we will present the original
FELICS framework.  We will then present FELICS-AE in
section~\ref{sec:felics-ae}, going over our work to adapt the
framework and explaining how to use it.  We will present the results
we obtained in section~\ref{sec:results}.  To conclude, we will
mention possible improvements for FELICS-AE in
section~\ref{sec:future}.

\section{Background: the FELICS framework}
\label{sec:felics}

The FELICS framework~\cite{FELICS:paper} includes a collection of
implementations of encryption algorithms in C and assembly, as well as
a set of shell scripts which measure the performance of these
algorithms on various microcontrollers representative of ``Internet of
Things'' (IoT) devices.

\subsection{Supported devices}
\label{sec:felics/devices}

FELICS supports the following microcontrollers (\textbf{bold} words
denote the codenames used within the framework):

\begin{itemize}
\item 8-bit \textbf{AVR} ATmega128,
\item 16-bit \textbf{MSP}430F1611,
\item 32-bit \textbf{ARM} Cortex-M3.
\end{itemize}

The AVR and MSP platforms are entirely simulated, which allows one to
measure algorithm performance on these microcontrollers without
physically owning them.  To measure performance on ARM, however,
FELICS requires an Arduino Due board, as well as a J-Link probe.
Algorithms can also be benchmarked on the implementer's x86 platform,
codenamed \textbf{PC}.

\subsection{Metrics}
\label{sec:felics/metrics}

\begin{description}
\item[Code size:] FELICS adds up the \texttt{text} and \texttt{data}
  sections of an implementation's compiled object code, as reported by
  the GNU \texttt{size} program, to measure the algorithm's footprint
  on non-volatile memory.

\item[RAM:] to measure the working memory needed by an algorithm, the
  framework runs the implementation through a debugger, spraying a
  known pattern on the stack before execution and counting how many
  bytes were modified after execution.  This figure is added to the
  object code's \texttt{data} section.

\item[Execution time:] for simulated devices, FELICS relies on the
  simulator to keep track of the number of clock cycles spent on
  encryption.  For other devices, FELICS uses specialized assembly
  instructions to get this information.
\end{description}
\subsection{Distribution}
\label{sec:felics/dist}

The CryptoLUX wiki~\cite{FELICS:wiki} hosts an archive containing
FELICS's source code (algorithm implementations and benchmarking
scripts).  The wiki also provides detailed instructions to install the
dependencies FELICS needs to compile implementations and measure their
performance on every platform.

The wiki also hosts a virtual machine (32-bit Ubuntu 14.04) where all
dependencies are pre-installed.  This makes it easier to use the
framework since one then does not need to track down all of its
dependencies.

\subsection{Algorithm instrumentation}
\label{sec:felics/adding-algos}

Algorithm implementations must comply with a number of requirements in
order to work with FELICS.  This section presents some of these
constraints.

\paragraph{Algorithm entry points must conform to a specific API.}

FELICS features multiple \emph{scenarios}, implemented as C files
which define the \texttt{main} function that will call the cipher
implementation.  Each scenario calls the cipher with different
parameter sizes, which allows observing the evolution of the
algorithm's performance as its input grows.

Each \texttt{main} function is generic with respect to the algorithm
under test: it is expected that each implementation defines high-level
encryption and decryption functions with specific signatures, so that
scenarios can be applied to all algorithms included in FELICS.

\paragraph{Encryption and decryption code must be split across
  distinct files.}

When measuring an algorithm's code size, FELICS outputs three distinct
tallies: encryption code size, decryption code size, and total code
size.  To achieve this, FELICS requires integrators to fill in
metadata files, spelling out which object files are used for
encryption, and which are used for decryption.

This means that if an implementation originally had one file featuring
both encryption and decryption functions, an integrator must split it
into two files and tell FELICS which file serves which purpose.  If
the original file contained code used by both encryption and
decryption functions, the integrator must further create a third file
to move the common code to.

\paragraph{Array declarations must be annotated.}

FELICS defines a set of macros that annotate integer types for two
purposes:

\begin{itemize}
\item They specify optimal memory alignment for integer arrays: this
  ensures that implementations aliasing byte arrays as e.g. 32-bit
  integer arrays do not accidentally access an array member at an
  address which is not aligned for a 32-bit variable, which can
  degrade performance or cause undefined behaviour.

\item
  \begin{sloppypar}
    They add the platform-dependent keywords necessary to tell the
    compiler whether arrays should be stored in ROM or RAM: e.g. for
    AVR \texttt{ROM\_DATA\_BYTE} expands to \texttt{const uint8\_t
    PROGMEM aligned}, where \texttt{PROGMEM} instructs \texttt{gcc} to
    move the variable to Program Memory;
    \texttt{READ\_ROM\_DATA\_BYTE} expands to
    \texttt{pgm\_read\_byte}, which performs the operations needed to
    read from this specific memory region.
  \end{sloppypar}
\end{itemize}

An integrator must therefore go over each array declaration in an
implementation and change its type using the correct macro.

\section{FELICS for Authenticated Encryption}
\label{sec:felics-ae}

FELICS was initially developed to measure the performance of block
ciphers and stream ciphers.  In this section, we describe the changes
we made to adapt the framework to AEAD algorithms; we then describe
how we use it.

\subsection{Changes from FELICS}
\label{sec:felics-ae/diff-felics}

We began our work with release 1.1.0 of the FELICS framework.  Our
goals were to support AEAD algorithms, simplify the algorithm
integration process, and improve the feedback given to implementers
while they optimize their code.

\subsubsection{AEAD support}
\label{sec:felics-ae/aead-support}

Authenticated encryption primitives have specific signatures: their
inputs include the associated data to authenticate, a nonce to achieve
semantic security~\cite{Rogaway:AEAD}; encryption produces an
authentication tag which is consumed by decryption.

The common C API we chose for cipher implementations is inspired by
the \emph{crypto\_aead} API described in the call for submissions of
the CAESAR competition~\cite{CAESAR:call}, which has been re-used in
the NIST standardization process~\cite{NIST:LWC-requirements}.  Our
API differs in minor ways:

\begin{itemize}
\item It does not include the \texttt{nsec} parameter, which is unused
  in the context of the NIST standardization process.  Removing this
  unused variable reduces the number of compiler warnings, which helps
  implementers spot actual errors in their code.
\item Arrays are passed as pointers to \texttt{uint8\_t} rather
  \texttt{unsigned char}.  Array sizes are passed as \texttt{size\_t}
  rather than \texttt{unsigned long long}.  The latter is
  unnecessarily large on some platforms: e.g. on AVR, an
  \texttt{unsigned long long} variable takes 8 bytes, while
  \texttt{size\_t} is only 2 bytes.
\end{itemize}

\subsubsection{Tools for implementation optimization}
\label{sec:felics-ae/tools-opt}

The original FELICS framework offers multiple entry points:

\begin{itemize}
\item one makefile for each implementation, which compiles a scenario
  for any platform, or runs a scenario on the development PC,
\item one script per metric, which runs the relevant tools
  (e.g. simulators, debuggers) for a given device and algorithm, and
  either displays the results in a human-readable table or serializes
  them in an unspecified format,
\item \texttt{collect\_ciphers\_metrics.sh}, a more complex script
  which iterates over algorithms, architectures, platforms, scenarios,
  and compiler options, and calls on the aforementioned scripts and
  makefiles to run a comprehensive measurement campaign.
\end{itemize}

While developing optimized implementations of \textsc{Lilliput-AE}, we
found that our main tasks were:

\begin{itemize}
\item running a full measurement campaign for a set of algorithms,
\item comparing a set of results against a previous set,
\item comparing two implementations of the same algorithm
  (e.g. reference vs. threshold, reference vs. optimized for a
  specific architecture),
\item exporting a set or a subset of results into various formats.
\end{itemize}

We developed a new set of scripts to perform these tasks, which we
present in section~\ref{sec:felics-ae/usage}.  We chose to implement
these tools in Python, which we found more convenient than Bash for
multiple reasons: e.g. a rich library ecosystem, reduced boilerplate
(the \texttt{argparse} library, for example, produces detailed usage
messages automatically, whereas the usage messages for the original
shell scripts must be maintained manually).

While adapting FELICS to AEAD algorithms and integrating
\textsc{Lilliput-AE}, \textsc{Ascon} and ACORN, we also stumbled on
several issues related to the poor error-reporting capabilities of
shell scripts:

\begin{itemize}
\item None of the framework's scripts uses the \texttt{errexit} shell
  option: command errors are only detected when they are followed by
  ad hoc checks.
\item These checks do not necessarily stop the script, which means
  that some failures cannot be detected unless the user either
  carefully watches the framework's output, or surveys the results
  closely enough to notice suspicious patterns (e.g. metrics set to
  zero).
\item Simply setting the \texttt{errexit} option only marginally
  improves the situation, since it does not produce a backtrace when
  an error happens: tracking down a failed command which produces no
  output involves a certain amount of manual work.  The situation is
  aggravated by the framework's error-checking convention, where
  commands writing to the standard error stream are assumed to have
  failed: naively setting the \texttt{xtrace} shell option in
  sub-scripts then causes spurious failures in the parent scripts.
\end{itemize}

Python exceptions interrupt the program flow immediately, include
precise backtraces, and may be enriched with arbitrary information by
the developer.  We found these properties to make error-handling more
ergonomic.

\subsubsection{Distribution}
\label{sec:felics-ae/distrib}

In order to make it easier to setup FELICS-AE, we wrote scripts to
fetch and install the framework's dependencies.  Building on those
scripts, we produced a ready-to-use Docker image bundling FELICS-AE
with its dependencies; the generation of this image is itself fully
scripted.

\subsubsection{Miscellaneous}
\label{sec:felics-ae/misc}

We made several changes that aimed at simplifying the framework,
removing degrees of freedom which we did not need.  For example, we
removed the ``scenario'' parameter; measurement campaigns now always
begin by checking the implementation against a test vector, then
measure the performance of encrypting 16 bytes of plaintext with 16
bytes of associated data.

We also removed \texttt{collect\_ciphers\_metrics.sh}'s support for
multiple output formats: we settled on JSON as a unique format for the
results of a measurement campaign.  All tools presented in
section~\ref{sec:felics-ae/usage} use this format as their input, and
a specific script is used to convert them to other formats.

Beyond encryption and decryption, FELICS further distinguished metrics
for key schedule (for block ciphers) and setup phase (for stream
ciphers).  Some AEAD algorithms call the underlying block cipher and
its key schedule repeatedly for every block of input; there was no
obvious way to adapt the framework to preserve this distinction, so
FELICS-AE only provides metrics for the whole encryption process.

While adapting the cycle-counting assembly code for x86\_64
architectures, we observed considerable variance in our figures for
execution time.  A lot of CPU features of modern workstations
contribute to this variance: to mitigate these factors, we implemented
several countermeasures, following Intel's guidelines for benchmarking
on IA-64 architectures~\cite{Intel:64bit-ISA-bench}:

\begin{itemize}
\item Use the \texttt{cpuid} instruction to ensure that all
  instructions are serialized correctly, otherwise out-of-order
  execution may move part of the code we want to benchmark out of the
  scope of the timestamp-measuring instructions, or even move
  unrelated code inside this scope.

\item Use the \texttt{taskset} command to pin the benchmark program to
  a single core, otherwise the code may be moved to other cores while
  it runs, and the timestamp counters of these cores may not be
  synchronized.

\item If the user is privileged enough, set the CPU frequency scaling
  governor for this core to ``performance'' to ensure a fixed
  frequency.

\item Run the scenario multiple times and take the median cycle count,
  to account for the remaining variance.
\end{itemize}

\subsection{Usage}
\label{sec:felics-ae/usage}

In this section, we will present some of the tools we developed while
adding support for AEAD algorithms to FELICS.

\subsubsection{\texttt{felics-run}}

This script expects a list of algorithms (each element of the list can
include wildcards to designate multiple algorithms), an optional set
of architectures (all supported platforms are selected by default),
and an optional list of compiler flags (only \texttt{-O3} is used by
default).

For every combination of the requested parameters, this script checks
the implementation's test vector, then measures the implementation's
code size, RAM usage and cycle count when encrypting 16 bytes of
plaintext with 16 bytes of associated data.

The metrics are serialized in a JSON file, along with some metadata to
identify the framework revision.

\subsubsection{\texttt{felics-publish}}

This script either prints every setup (i.e. a given set of algorithm,
architecture, and compiler options) from a results file to the user's
console, or exports these setups into a new output format.  The
supported formats are HTML table, \LaTeX{} table, OOXML spreadsheet or
ODF spreadsheet.

Several options allow the user to control the output:

\begin{description}
\item[\texttt{-{}-sort-by}:] how setups are ordered,
\item[\texttt{-{}-filter}:] which setups are included,
\item[\texttt{-{}-info}:] which metadata and metrics are displayed,
\item[\texttt{-{}-table-label}:] anchor for documents supporting
  cross-references,
\item[\texttt{-{}-table-caption}:] additional text to describe the
  data set.
\end{description}

Sample console output:

\begin{verbatim}
On AVR
Lilliput-I-128 (felicsref, -O3):  6100 266 129093
Lilliput-II-128 (felicsref, -O3): 6062 243 132650

On MSP
Lilliput-I-128 (felicsref, -O3):  5760 300 121646
Lilliput-II-128 (felicsref, -O3): 4932 272 144399

On ARM
Lilliput-I-128 (felicsref, -O3):  4656 444 86293
Lilliput-II-128 (felicsref, -O3): 4684 420 89390

On PC
Lilliput-I-128 (felicsref, -O3):  6880 528 10030
Lilliput-II-128 (felicsref, -O3): 6783 528 11816
\end{verbatim}

\subsubsection{\texttt{felics-compare}}

This script iterates over every setup found in one results file,
computes the performance ratio with respect to the same setups in a
second results file, and highlights these ratios so that the
implementer can judge at a glance whether a code change had a positive
or negative impact on performance.

Sample output:

\begin{Verbatim}[
  commandchars=\\\{\},
  codes={\catcode`\$=3},
]
Comparing
	foo.json
	(master) 1234567 Commit summary foo
against
	bar.json
	(master) 89abcde Commit summary bar

Lilliput-I-128 on AVR (vfelicsref with -Os)
	code_size: \textcolor{green}{-12.19%} (3166 $\searrow$ 2780)
	code_ram: \textcolor{green}{-49.22%} (514 $\searrow$ 261)
	code_time: \textcolor{red}{+32.05%} (189818 $\nearrow$ 250657)

Lilliput-I-192 on AVR (vfelicsref with -Os)
	code_size: \textcolor{green}{-10.47%} (3268 $\searrow$ 2926)
	code_ram: \textcolor{green}{-50.71%} (562 $\searrow$ 277)
	code_time: \textcolor{red}{+36.73%} (230309 $\nearrow$ 314893)

Lilliput-I-256 on AVR (vfelicsref with -Os)
	code_size: \textcolor{green}{-8.25%} (3392 $\searrow$ 3112)
	code_ram: \textcolor{green}{-53.19%} (626 $\searrow$ 293)
	code_time: \textcolor{red}{+40.83%} (290363 $\nearrow$ 408907)
\end{Verbatim}

The user can choose to ignore minor differences by providing a
\texttt{-{}-threshold} argument; ratios lower than this threshold will
be hidden.

\begin{sloppypar}
  Other scripts perform similar comparisons: if the FELICS-AE
  directory is version-controlled,
  \textbf{\texttt{felics-compare-revisions}} automatically checks out
  two revisions, runs the requested benchmarks, and compares their
  results files.  \textbf{\texttt{felics-compare-implementations}}
  pairs comparable setups (architecture, compiler options) for two
  implementations in the same results file, and compares their
  metrics.
\end{sloppypar}

\section{Results}
\label{sec:results}

This section comments on our measurements of the performance of
\textsc{Lilliput-AE} and the members of the final portfolio of the
CAESAR competition for the ``lightweight''
use-case~\cite{CAESAR:submissions}, \textsc{Ascon} and ACORN.

\subsection{Setup}
\label{sec:results/setup}

The measurements were performed on an Ubuntu 16.04 64-bit desktop with
4 \SI{3.5}{\GHz} CPUs and \SI{8}{\giga \byte} RAM.  The software
versions for platform-specific compilers, debuggers and other such
utilities correspond to those distributed by Ubuntu, with the
exception of software listed in table~\ref{table:results/setup}.

\begin{table}[H]
  \centering
  \begin{tabular}{l|l|l|l}
    \textbf{Platform} & \textbf{Software} & \textbf{Version}         & \textbf{Origin}                               \\
    \hline
    \multirow{2}{*}{AVR}
                      & simavr            & \texttt{v1.6}            & Developer release~\cite{FELICS:simavr}        \\
                      & Avrora            & \texttt{1.7.117-patched} & Cf. FELICS documentation~\cite{FELICS:avrora} \\
    \hline
    \multirow{2}{*}{MSP}
                      & MSP430-GCC        & \texttt{7.3.2.154}       & Texas Instruments~\cite{FELICS:msp430-gcc}    \\
                      & MSPDebug          & \texttt{v0.25}           & Developer release~\cite{FELICS:mspdebug}      \\
    \hline
    \multirow{1}{*}{ARM}
                      & J-Link Software   & \texttt{V6.42f}          & SEGGER~\cite{FELICS:jlink-soft}               \\
    \hline
  \end{tabular}
  \caption{Software versions for the FELICS-AE framework.}
  \label{table:results/setup}
\end{table}

We considered two compilation options:

\begin{itemize}
\item \texttt{-03}, to minimize computation time and decrease power
  consumption,
\item \texttt{-Os}, to reduce code size and thus optimize for low
  memory footprint.
\end{itemize}

\subsection{Implementations}
\label{sec:results/implementations}

The source code for the CAESAR algorithms was adapted from the
SUPERCOP~\cite{CAESAR:SUPERCOP} framework.  In order to provide a fair
assessment of each algorithm's performance, we looked for
implementations that performed well (i.e. better than the reference
version) for each FELICS platform.  Table~\ref{table:results/implems}
sums up which implementations were considered for each platform.

\begin{table}[H]
  \centering
  \begin{tabular}{l|l|l}
    \textbf{Algorithm} & \textbf{Platform} & \textbf{Implementations}     \\
    \hline
    \multirow{4}{*}{\textsc{Ascon}}
                       & AVR               & \texttt{ref}                 \\
                       & MSP               & \texttt{ref}                 \\
                       & ARM               & \texttt{ref}, \texttt{opt32} \\
                       & PC                & \texttt{ref}, \texttt{opt64} \\
    \hline
    \multirow{4}{*}{\textsc{ACORN}}
                       & AVR               & \texttt{8bitfast}            \\
                       & MSP               & \texttt{8bitfast}            \\
                       & ARM               & \texttt{opt1}                \\
                       & PC                & \texttt{opt1}                \\
    \hline
  \end{tabular}
  \caption{Algorithm implementations for each platform.}
  \label{table:results/implems}
\end{table}

For \textsc{Lilliput-AE}, we used the same implementation on all
platforms.  This implementation, called \texttt{felicsref}, closely
resembles the reference implementation, except for a few tweaks
documented in each source file's header comments.

\subsection{Discussion}

We published our measurements in our submission to the first round of
the LWC standardization process~\cite{NIST:Lilliput-AE}.  We also
published these figures on the \textsc{Lilliput-AE}
website~\cite{PACLIDO:Lilliput-AE}, where we intend to add results for
optimized versions of \textsc{Lilliput-AE}, as well as for other
candidates to the standardization process.

First, we compare the performance of \textsc{Ascon} and ACORN to the
128-bit key variants of \textsc{Lilliput-AE}:

\begin{description}
\item[On 8-bit AVR,] when compiled for speed, both variants of
  \textsc{Lilliput-AE} compare favorably to \textsc{Ascon} and ACORN,
  even in terms of code size and RAM usage: the only unfavorable
  comparison is ACORN's lower code size.  When compiled with
  \texttt{-Os}, \textsc{Lilliput-AE} variants achieve the lowest code
  size, though \textsc{Ascon} and ACORN become faster.

\item[On 16-bit MSP,] when compiled for speed, \textsc{Lilliput-AE}
  variants come well ahead in terms of cycle count, though again ACORN
  has a lower code size.  When compiled with \texttt{-Os},
  \textsc{Lilliput-AE} variants are the lightest and the fastest,
  though ACORN has a slightly smaller RAM footprint.

\item[On 32-bit ARM and 64-bit PC,] whichever compilation option we
  consider, \textsc{Lilliput-AE} variants are the slowest algorithms;
  in terms of code size, they compare unfavorably to the reference
  version of \textsc{Ascon}.
\end{description}

Next, we study the impact of the choice of key length (128, 192 or
256) and AE mode ($\Theta$CB3 for \textsc{Lilliput-I}, SCT-2 for
\textsc{Lilliput-II}) on the performance of \textsc{Lilliput-AE}:

\begin{itemize}
\item Longer keys imply more tweakey \emph{lanes} (64-bit words).  The
  key schedule updates each lane with a different matrix
  multiplication, therefore longer keys lead to a higher memory
  footprint, as well as more cycles.  This may explain why SCT-2
  variants have the smallest memory footprint: they use 128-bit
  tweaks, whereas $\Theta$CB3 variants use 192-bit tweaks.

\item $\Theta$CB3 variants, on the other hand, are consistently faster
  than SCT-2 variants.  This may be due to $\Theta$CB3 making fewer
  calls to the underlying block cipher $E_K$: for an $l$-block
  plaintext, $\Theta$CB3 calls $E_K$ $l$ times, while SCT-2 calls
  $E_K$ $2l$ times.
\end{itemize}

\section{Future work}
\label{sec:future}

In this section, we identify ways in which FELICS-AE could be
improved: new benchmarking features, simplifications to make
extensions easier, structural cleanups, etc.

\paragraph{Simplify the addition of new devices.}

The plumbing required to support a microcontroller architecture is
scattered across several files: e.g. shell and debugger scripts to
measure metrics, makefiles to handle compilation, C code to provide
device-specific functions.  Adding a new platform is very much a
trial-and-error process; re-organizing this infrastructure code would
go a long way toward smoothing the steps out.

\paragraph{Simplify the addition of new algorithms.}

For example, no source file can contain both encryption-specific and
decryption-specific code: since FELICS-AE only measures the size of
whole object files, the size of the decryption-specific code will be
added to the tally for encryption, and vice versa.

To remove this restriction, user-supplied metadata could be enriched
to specify symbols (functions or data) that are specific to one
operation; the size of these symbols would be subtracted from the
irrelevant tallies.  Alternatively, such symbols could be annotated
with the compiler attribute \texttt{section}: code-size measurement
scripts could then assume that symbols in a specific ELF section are
only used for one operation.

\paragraph{Allow multiple scenarios.}

The original FELICS framework allowed metrics to be collected for
various use-cases.  Adding this capability back could be as simple as
adding \texttt{-{}-plaintext-size} and \texttt{-{}-adata-size}
parameters to \texttt{felics-run}, and recording the value of these
parameters in the results file.

\paragraph{Allow multiple test vectors.}

AEAD algorithms may contain branching code paths, e.g. to handle
padding.  A single test vector cannot cover both sides of a branch;
implementers may therefore have no way to realize that their
optimizations break the code for some inputs.

\paragraph{Add profiling}

The original FELICS framework allowed the user to profile an algorithm
in some limited way, by hooking into specific functions such as a
block cipher's key schedule.  It may be possible to generalize this
feature using traditional profiling tools such as \texttt{gprof}.

\bibliographystyle{plain}
\bibliography{references}

\end{document}
