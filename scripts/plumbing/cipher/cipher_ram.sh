#!/bin/bash

#
# University of Luxembourg
# Laboratory of Algorithmics, Cryptology and Security (LACS)
#
# FELICS - Fair Evaluation of Lightweight Cryptographic Systems
#
# Copyright (C) 2015 University of Luxembourg
#
# Written in 2015 by Daniel Dinu <dumitru-daniel.dinu@uni.lu>
#
# This file is part of FELICS.
#
# FELICS is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# FELICS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

#
# Call this script to extract the cipher RAM consumption
# 	./cipher_ram.sh [{-a|--architecture}=[PC|AVR|MSP|ARM]] [{-o|--output}=[...]]
#
#	To call from a cipher build folder use:
#		./../../../../scripts/cipher/cipher_ram.sh [options]
#
#	Options:
#		-a, --architecture
#			Specifies which architecture to build for
#				Default: PC
#		-o, --output
#			Specifies where to output the results. The relative path is computed from the directory where script was called
#				Default: /dev/tty
#
#	Examples:
#		./../../../../scripts/cipher/cipher_ram.sh
#		./../../../../scripts/cipher/cipher_ram.sh --architecture=MSP
#  		./../../../../scripts/cipher/cipher_ram.sh -o=results.txt
#

set -eu

# Get current script path
script_path=$(dirname $0)

# Include configuration file
source $script_path/../config.sh

# Include constants files
source $script_path/../constants/constants.sh

MEMORY_PATTERN=(0x11 0x22 0x33 0x44 0x55 0x66 0x77 0x88 0x99 0xAA)
MEMORY_FILE=memory.mem
MEMORY_SIZE=2000


# Default values
SCRIPT_ARCHITECTURE=$SCRIPT_ARCHITECTURE_PC
SCRIPT_OUTPUT=$DEFAULT_SCRIPT_OUTPUT


# Parse script arguments
for i in "$@"
do
	case $i in
		-a=*|--architecture=*)
			SCRIPT_ARCHITECTURE="${i#*=}"
			shift
			;;
		-o=*|--output=*)
			if [[ "${i#*=}" ]] ; then
				SCRIPT_OUTPUT="${i#*=}"
			fi
			shift
			;;
		*)
			# Unknown option
			;;
	esac
done


echo "Script settings:"
echo -e "\t SCRIPT_ARCHITECTURE \t\t = $SCRIPT_ARCHITECTURE"
echo -e "\t SCRIPT_OUTPUT \t\t\t = $SCRIPT_OUTPUT"


GDB_OUTPUT_FILE=${SCRIPT_ARCHITECTURE}_gdb_stack_sections.log

# Run or simulate the binary under a debugger.
run-debugger ()
{
	local binary=felics_bench.elf
	local command_file=../../../../scripts/plumbing/cipher/stack/${SCRIPT_ARCHITECTURE,,}_stack_sections.gdb
	local make_log_file=${SCRIPT_ARCHITECTURE}_cipher_ram_make.log

	local -A servers=(
		[AVR]=simavr
		[MSP]=mspdebug
		[ARM]=JLinkGDBServer
		[NRF52840]=JLinkGDBServer
		[STM32L053]=st-util
		[PC]=N/A
	)
	local server_log=${SCRIPT_ARCHITECTURE}_${servers[${SCRIPT_ARCHITECTURE}]}_stack_sections.log

	echo "Run GDB script $(basename ${command_file})"

	case $SCRIPT_ARCHITECTURE in
		$SCRIPT_ARCHITECTURE_PC)
			$PC_GDB -x $command_file -tty /dev/null ${binary} &> $GDB_OUTPUT_FILE
			;;
		$SCRIPT_ARCHITECTURE_AVR)
			$SIMAVR_SIMULATOR -g -m atmega128 ${binary} &> ${server_log} &
			$AVR_GDB -x $command_file &> $GDB_OUTPUT_FILE

			kill -INT %'$SIMAVR_SIMULATOR'
			wait %'$SIMAVR_SIMULATOR'
			;;
		$SCRIPT_ARCHITECTURE_MSP)
			local commands=(
				"prog ${binary}"
				"simio add hwmult hwmult"
				gdb
			)
			$MSPDEBUG_SIMULATOR -n sim "${commands[@]}" &> ${server_log} &
			$MSP_GDB -x $command_file &> $GDB_OUTPUT_FILE

			kill -INT %'$MSPDEBUG_SIMULATOR'
			wait %'$MSPDEBUG_SIMULATOR'
			;;
		$SCRIPT_ARCHITECTURE_ARM)
			# Upload the program to the board
			make -f ./../../../common/cipher.mk ARCHITECTURE=$SCRIPT_ARCHITECTURE upload-bench &> $make_log_file

			$JLINK_GDB_SERVER -device cortex-m3 &> ${server_log} &
			$ARM_GDB -x $command_file &> $GDB_OUTPUT_FILE

			kill -INT %'$JLINK_GDB_SERVER'
			wait %'$JLINK_GDB_SERVER'
			;;
		$SCRIPT_ARCHITECTURE_NRF52840)
			# Upload the program to the board
			make -f ./../../../common/cipher.mk ARCHITECTURE=$SCRIPT_ARCHITECTURE upload-bench &> $make_log_file

			$JLINK_GDB_SERVER -device NRF52840_XXAA -if SWD -speed 4000 &> ${server_log} &
			$NRF52840_GDB -x $command_file &> $GDB_OUTPUT_FILE

			kill -INT %'$JLINK_GDB_SERVER'
			wait %'$JLINK_GDB_SERVER'
			;;
		$SCRIPT_ARCHITECTURE_STM32L053)
			# Upload the program to the board
			make -f ./../../../common/cipher.mk ARCHITECTURE=$SCRIPT_ARCHITECTURE upload-bench &> $make_log_file

			$STLINK_GDB_SERVER &> ${server_log} &
			$STM32L053_GDB -x $command_file &> $GDB_OUTPUT_FILE

			kill -INT %'$STLINK_GDB_SERVER'
			wait %'$STLINK_GDB_SERVER'
			;;
	esac
}


# Compute the stack usage
# Parameters:
# 	$1 - the gdb output file
# 	$2 - the gdb printed variable name
function compute_stack_usage()
{
	local output_file=$1
	local variable_name=$2

	# Get the stack content array
	local stack_content=( $(cat $output_file | awk '/\$'$variable_name' = {/,/}/' | tr -d '\r' | cut -d '{' -f 2 | cut -d '}' -f 1 | tr -d ',') ) 

	local count=0
	while [ $((${stack_content[$count]})) -eq $((${MEMORY_PATTERN[$(($count % $memory_patern_length))]})) ]
	do
		count=$(($count + 1));
	done

	local used_stack=$(($MEMORY_SIZE - $count))
	
	echo $used_stack
}


echo "Begin cipher RAM - $(pwd)"


get-api-constant ()
(
    "${script_path}"/cipher_constant.sh .. $1
)

get-implem-info ()
{
    local key=$1
    grep "^${key}: " ../source/implementation.info | cut -d' ' -f2-
}

key_size=$(get-api-constant CRYPTO_KEYBYTES)

# Set the searched files pattern
pattern='*.o'

# Get the number of files matching the pattern
files_number=$(find . -maxdepth 1 -type f -name "$pattern" | wc -l)

if [ 0 -eq $files_number ] ; then
	echo "There is no file matching the pattern: '$pattern' for cipher '$cipher_name'!"
	exit 1
fi

for file in felics_bench.elf ${pattern}
do
	# Get the section sizes line for current file
	if [ -e $file ] ; then
		size=$(size $file | grep $file)
	else
		continue
	fi

	# Get the section data size
	data=$(echo $size | cut -d ' ' -f 2)
	
	# Get the component name: remove filename extension, replace
	# characters unfit for variable names.
	component=${file%.*}
	component=${component//-/__}
	declare $component"_data"=$data
done


shared_constants_e=0
shared_constants_d=0
shared_constants_total=0

# Read and process constants implementation information
declare -a shared_parts
for section in EncryptCode DecryptCode
do
	shared_files=$(get-implem-info ${section} | tr ',' ' ')

	for shared_file in $shared_files
	do
		component=${shared_file%%\!*}
		component=${component//-/__}
		shared_name=${component}"_data"

		shared_value=${!shared_name}
		if [ "" == "$shared_value" ] ; then
			shared_value=0
		fi


		# Test if the shared file RAM was added to the total
		used_part=$FALSE
		for shared_part in ${shared_parts[@]}
		do
			if [ "$shared_part" == "$shared_file" ] ; then
				used_part=$TRUE
				break
			fi
		done

	
		# Add the shared file ROM to total
		if [ $FALSE -eq $used_part ]; then
			shared_constants_total=$(($shared_constants_total + $shared_value))
			shared_parts+=($shared_file) 
		fi
	
	
		case $section in
			EncryptCode)
				shared_constants_e=$(($shared_constants_e + $shared_value))
				;;
			DecryptCode)
				shared_constants_d=$(($shared_constants_d + $shared_value))
				;;
		esac
	done
done


# Compute the data RAM
data_ram_e=$shared_constants_e
data_ram_d=$shared_constants_d

grep-define ()
{
    local var=$1
    local src=../../../common/felics/main_bench.c
    grep -E "^#define +${var} +[0-9]+$" ${src} | grep -Eo '[0-9]+'
}

data_size=$(grep-define DATA_SIZE)
associated_data_size=$(grep-define ASSOCIATED_DATA_SIZE)
data_ram_common=$(($key_size + $data_size + $associated_data_size))
data_ram_total=$(($data_ram_common + $shared_constants_total))


# Get the memory pattern length
memory_patern_length=$((${#MEMORY_PATTERN[@]}))

# Generate the memory file
memory_file=$SCRIPT_ARCHITECTURE$FILE_NAME_SEPARATOR$MEMORY_FILE
echo "Generate the memory file: '$memory_file'"
echo -n "" > $memory_file

for ((i=0; i<$MEMORY_SIZE/$memory_patern_length; i++))
do
	echo -ne "$(printf '\\x%x\\x%x\\x%x\\x%x\\x%x\\x%x\\x%x\\x%x\\x%x\\x%x' ${MEMORY_PATTERN[*]})" >> $memory_file
done


run-debugger

e_stack=$(compute_stack_usage $GDB_OUTPUT_FILE 1)
d_stack=$(compute_stack_usage $GDB_OUTPUT_FILE 2)

# Display results
printf "%s %s %s %s %s %s" $e_stack $d_stack $data_ram_e $data_ram_d $data_ram_common $data_ram_total > $SCRIPT_OUTPUT
	

echo ""
echo "End cipher RAM - $(pwd)"
